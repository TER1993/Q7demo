package com.spd.qsevendemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.spd.qsevendemo.adapter.DataAdapter;
import com.spd.qsevendemo.model.DataBean;
import com.spd.qsevendemo.model.DatabaseAction;
import com.spd.qsevendemo.model.DatabaseManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author xuyan  展示画面
 */
public class DataActivity extends AppCompatActivity {

    private DataAdapter mAdapter;
    private List<DataBean> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        initView();
    }

    private void initView() {
        mList = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rv_content);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        //列表再底部开始展示，反转后由上面开始展示
        layout.setStackFromEnd(true);
        //列表翻转
        layout.setReverseLayout(true);
        recyclerView.setLayoutManager(layout);
        mAdapter = new DataAdapter(mList);
        recyclerView.setLayoutManager(new LinearLayoutManager(AppSeven.getInstance()));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mList = DatabaseAction.queryData();
        Collections.reverse(mList);
        mAdapter.setNewData(mList);
    }

    @Override
    protected void onDestroy() {
        if (mList.size() > 5000) {
            DatabaseManager.getInstance().getDao().getDataBeanDao().deleteAll();
        }
        super.onDestroy();
    }
}
