package com.spd.qsevendemo;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

import com.spd.qsevendemo.measure.view.GLPanel;
import com.spd.qsevendemo.model.SevenBean;
import com.spd.qsevendemo.utils.FileUtils;
import com.spd.qsevendemo.utils.Logcat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

import static com.spd.qsevendemo.utils.HnweUtils.SD_PROP_C128_ENABLED;
import static com.spd.qsevendemo.utils.HnweUtils.SD_PROP_C39_ENABLED;
import static com.spd.qsevendemo.utils.HnweUtils.SD_PROP_QR_ENABLED;
import static com.spd.qsevendemo.utils.HnweUtils.SD_PROP_UPC_ENABLED;

/**
 * @author xuyan 将MainActivity中方法提取出来
 */
public class SevenApi {

    /**
     * 以1920*1080为样例
     */
    private int mWidth = 1920;
    private int mHeight = 1080;

    /**
     * 要保存的条码及对应int值以及状态。直接整个保存。
     */
    private static String[] codename = {"CODE128", "CODE39", "UPC", "QR"};

    private static int[] codenumber = {SD_PROP_C128_ENABLED, SD_PROP_C39_ENABLED, SD_PROP_UPC_ENABLED, SD_PROP_QR_ENABLED};

    /**
     * 返回设置页面的条码使能显示
     *
     * @return 条码使能列表
     */
    public static List<SevenBean> getSevenList() {
        List<SevenBean> mList = new ArrayList<>();
        for (int i = 0; i < codename.length; i++) {
            SevenBean sevenBean = new SevenBean();
            sevenBean.setName(codename[i]);
            sevenBean.setCode(codenumber[i]);
            sevenBean.setCheck(false);
            mList.add(sevenBean);
        }
        return mList;
    }


    /**
     * 删除文件
     */
    public static void delFile(final Context context) {
        Observable.create((ObservableOnSubscribe<String>) e -> {
            try {
                boolean b = FileUtils.deleteFile("data/data/"
                        + FileUtils.getAppProcessName(context) + "/anchoring-0");
                boolean b1 = FileUtils.deleteFile("data/data/"
                        + FileUtils.getAppProcessName(context) + "/storage-0");
                Log.d("ZM", "删除文件: " + b + b1);
            } catch (Exception e1) {
                e1.printStackTrace();
                Log.d("ZM", "删除文件: " + e1.toString());
            }
        })
                .subscribeOn(Schedulers.newThread()).subscribe();
    }


    /**
     * 控制灯光
     */
    private static BufferedWriter TorchFileWrite;

    /**
     * @param str 数据
     */
    public void openCloseFlash(int cam, String str) {
        switch (cam) {
            case 0:
                //后置
                break;
            case 1:
                //前置
                break;
            case 2:
                //扫头
                Logcat.d("-print-rece-" + "openCloseFlash " + str + " start");
                File TorchFileName = new File("/sys/class/misc/lm3642/torch");
                try {
                    TorchFileWrite = new BufferedWriter(new FileWriter(TorchFileName, false));
                    TorchFileWrite.write(str);
                    TorchFileWrite.flush();
                    TorchFileWrite.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Logcat.d(e.getMessage());
                }
                Logcat.d("-print-rece-" + "openCloseFlash end");
                break;
            default:
                break;
        }
    }


    /**
     * 成功识别的条码框，通过坐标获取位置
     */
    private void initDrawView(BarcodeDrawView drawView, List<BarcodeBounds> mList2, RelativeLayout mLayout) {
        int[][] kuang;
        BarcodeBounds barcodeBounds;
        kuang = com.spd.code.CodeUtils.GetBounds();
        if (kuang == null) {
            return;
        }
        mList2 = new ArrayList<>();
        for (int i = 0; i < kuang.length; i++) {
            barcodeBounds = new BarcodeBounds(kuang[i], mWidth, mHeight);
            mList2.add(barcodeBounds);
        }
        if (drawView != null) {
            mLayout.removeView(drawView);
        }
        drawView = new BarcodeDrawView(AppSeven.getInstance(), mList2);
        mLayout.addView(drawView);
    }

    /**
     * 渲染RGB
     *
     * @param bufferRGB **
     * @param glPanel   **
     */
    private void drawColor(ByteBuffer bufferRGB, GLPanel glPanel) {
        if (glPanel != null) {
            glPanel.paint(null, bufferRGB, mWidth, mHeight);
        }
    }

//    /**
//     * 隐藏状态栏和导航栏，可以拉出，自动收起
//     */
//    public void setStatusNavigation() {
//        View decorView = getWindow().getDecorView();
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//    }





}
