package com.spd.qsevendemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.serialport.DeviceControlSpd;
import android.support.v7.app.AppCompatActivity;

import com.spd.qsevendemo.utils.HnweUtils;
import com.spd.qsevendemo.utils.LicenseUtil;
import com.spd.qsevendemo.utils.SpUtils;

import java.io.IOException;

import static com.spd.qsevendemo.model.SevenModel.LIGHT_LEVEL;

/**
 * @author xuyan 启动首页面，进入下一页面前初始化上电
 */
public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        initLicense();
        initDevice();
        initScanTime();
    }

    private void initLicense() {
        String path = Environment.getExternalStorageDirectory().getPath() + "/IMI";
        String name = "license";
        LicenseUtil.copy(AppSeven.getInstance(), name, path, name);
    }

    private DeviceControlSpd deviceControl;

    private void initDevice() {
        try {
            deviceControl = new DeviceControlSpd(DeviceControlSpd.POWER_NEWMAIN);
            deviceControl.newSetGpioOn(21);
            deviceControl.newSetGpioOn(57);
            deviceControl.newSetGpioOn(16);
            deviceControl.newSetGpioOn(14);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int level = (int) SpUtils.get(AppSeven.getInstance(), LIGHT_LEVEL, 3);
        HnweUtils.openCloseFlash(level);
    }

    private void initScanTime() {

        Handler handler = new Handler();
        handler.postDelayed(() -> {
            /*
             *要执行的操作
             */
            startActivity(new Intent(FirstActivity.this, MainActivity.class));
            try {
                deviceControl.DeviceClose();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finish();
            //3秒后执行Runnable中的run方法,否则初始化失败
        }, 3000);
    }

}
