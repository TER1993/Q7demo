package com.spd.qsevendemo.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author xuyan  保存历史记录
 */
@Entity
public class DataBean {
    private String id;
    private String weight;
    private String length;
    private String width;
    private String height;
    private String tiji;
    @Id
    private long time;
    @Generated(hash = 124701605)
    public DataBean(String id, String weight, String length, String width,
            String height, String tiji, long time) {
        this.id = id;
        this.weight = weight;
        this.length = length;
        this.width = width;
        this.height = height;
        this.tiji = tiji;
        this.time = time;
    }
    @Generated(hash = 908697775)
    public DataBean() {
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getWeight() {
        return this.weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getLength() {
        return this.length;
    }
    public void setLength(String length) {
        this.length = length;
    }
    public String getWidth() {
        return this.width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getHeight() {
        return this.height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    public String getTiji() {
        return this.tiji;
    }
    public void setTiji(String tiji) {
        this.tiji = tiji;
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }



}
