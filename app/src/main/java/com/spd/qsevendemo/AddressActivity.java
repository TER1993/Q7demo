package com.spd.qsevendemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.spd.qsevendemo.utils.SpUtils;
import com.spd.qsevendemo.utils.ToastUtils;

import static com.spd.qsevendemo.net.Urls.BASE_URL;

/**
 * @author xuyan
 */
public class AddressActivity extends AppCompatActivity {

    private EditText cd3MainUrl;
    //private TextView cd3UploadUrl;
    private TextView urlSure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        initView();
    }

    private void initView() {
        cd3MainUrl = findViewById(R.id.cd3MainUrl);
        //cd3UploadUrl = findViewById(R.id.cd3UploadUrl);
        urlSure = findViewById(R.id.urlSure);
        cd3MainUrl.setText((String) SpUtils.get(AppSeven.getInstance(), "appseven_url", BASE_URL));
        //cd3UploadUrl.setText(UPLOAD);
        urlSure.setOnClickListener(v -> {
            String one = cd3MainUrl.getText().toString();
            //String two = cd3UploadUrl.getText().toString();

            SpUtils.put(AppSeven.getInstance(), "appseven_url", one);

            ToastUtils.showShortToastSafe("设置url成功");
            //SpUtils.put(AppSeven.getInstance(), "appseven_method", two);
        });
    }
}