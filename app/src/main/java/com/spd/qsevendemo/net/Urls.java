package com.spd.qsevendemo.net;

/**
 * @author xuyan
 */
public class Urls {
    //public static final String BASE_URL = "http://192.168.1.90:8080/dragonparcel/tms/tmsOrders/";
    public static final String BASE_URL = "https://prod.dragonparcel.com/dragonparcel/tms/tmsOrders/";
    /**
     * 数据上传
     */
    public static final String UPLOAD = "measure";

}
